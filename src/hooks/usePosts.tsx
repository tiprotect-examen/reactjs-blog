import { useEffect, useState } from 'react';
import { IPostPaginatedResponse } from '../interfaces/postPaginatedResponse';
import { fetchPaginate } from '../services/postService';

export const usePosts = (page: number, search: string) => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [res, setRes] = useState<IPostPaginatedResponse>();

  useEffect(() => {
    fetchPaginate(page, search).then((resp: IPostPaginatedResponse) => {
      setRes(resp);
      setIsLoading(false);
    });
  }, [page, search]);

  return {
    isLoading,
    res,
  };
};
