import { useEffect, useState } from 'react';
import { IPost } from '../interfaces/postPaginatedResponse';
import { fetchPost } from '../services/postService';

export const usePost = (id: string) => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [post, setPost] = useState<IPost>();

  useEffect(() => {
    fetchPost(id).then((resp: IPost) => {
      setPost(resp);
      setIsLoading(false);
    });
  }, [id]);

  return { post, isLoading };
};
