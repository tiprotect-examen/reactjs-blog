export interface IPostPaginatedResponse {
  data: IPost[];
  meta: IMeta;
}

export interface IPost {
  id: number;
  title: string;
  author: string;
  content: string;
  created_at: Date;
}


export interface IMeta {
  current_page: number;
  from: number;
  last_page: number;
  path: string;
  per_page: number;
  to: number;
  total: number;
}
