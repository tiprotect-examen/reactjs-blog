import React, { useState } from 'react';
import { Form, Button, Spinner } from 'react-bootstrap';
import { useFormik } from 'formik';
import { addPost } from '../services/postService';
import { useNavigate } from 'react-router-dom';

type Props = {};

type FormFields = {
  title?: string;
  author?: string;
  content?: string;
};

const validate = (values: FormFields) => {
  //   const errors: FormFields = { title: null, author: null, content: null };
  const errors: FormFields = {};

  if (!values.title) {
    errors.title = 'El título es obligatorio';
  }

  if (!values.author) {
    errors.author = 'El autor es obligatorio';
  }

  if (!values.content) {
    errors.content = 'El contenido es obligatorio';
  }

  return errors;
};

const NewPost = (props: Props) => {
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const formik = useFormik({
    initialValues: {
      title: '',
      author: '',
      content: '',
    },
    validate,
    onSubmit: (values) => {
      setIsLoading(true);
      addPost(values)
        .then((resp) => {
          setIsLoading(false);
          navigate('/');
        })
        .catch((error) => {
          console.log(error);
          setIsLoading(false);
        });
    },
  });

  return (
    <Form onSubmit={formik.handleSubmit}>
      <Form.Group className="mb-3">
        <Form.Label>Título</Form.Label>
        <Form.Control
          type="text"
          placeholder="Ingrese el título del post"
          id="title"
          name="title"
          onChange={formik.handleChange}
          value={formik.values.title}
        />
        {formik.errors.title ? (
          <Form.Text className="text-muted">{formik.errors.title}</Form.Text>
        ) : null}
      </Form.Group>

      <Form.Group className="mb-3">
        <Form.Label>Autor</Form.Label>
        <Form.Control
          type="text"
          placeholder="Ingrese el autor del post"
          id="author"
          name="author"
          onChange={formik.handleChange}
          value={formik.values.author}
        />
        {formik.errors.author ? (
          <Form.Text className="text-muted">{formik.errors.author}</Form.Text>
        ) : null}
      </Form.Group>

      <Form.Group className="mb-3">
        <Form.Label>Contenido</Form.Label>
        <Form.Control
          as="textarea"
          rows={3}
          placeholder="Ingrese el autor del post"
          id="content"
          name="content"
          onChange={formik.handleChange}
          value={formik.values.content}
        />
        {formik.errors.content ? (
          <Form.Text className="text-muted">{formik.errors.content}</Form.Text>
        ) : null}
      </Form.Group>

      <Button variant="primary" type="submit" disabled={isLoading}>
        {isLoading ? (
          <Spinner
            as="span"
            className="mr-2"
            animation="grow"
            size="sm"
            role="status"
            aria-hidden="true"
          />
        ) : null}
        Enviar
      </Button>
    </Form>
  );
};

export default NewPost;
