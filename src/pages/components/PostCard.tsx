import { format } from 'date-fns';
import { Col, Card } from 'react-bootstrap';
import { IPost } from '../../interfaces/postPaginatedResponse';
import { Link } from 'react-router-dom';

type PostProp = {
  post: IPost;
};

export const PostCard = ({ post }: PostProp) => {
  return (
    <Col>
      <Card>
        <Card.Body>
          <Card.Title>{post.title}</Card.Title>
          <Card.Subtitle className="mb-2 text-muted">
            {post.author}
          </Card.Subtitle>
          <Card.Text>{post.content}</Card.Text>
          <Link to={`/${post.id}`}>Ver mas...</Link>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">
            {format(new Date(post.created_at), 'yyyy-MM-dd h:ii a')}
          </small>
        </Card.Footer>
      </Card>
    </Col>
  );
};
