import { ChangeEvent, useState } from 'react';
import { Row, Pagination, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { usePosts } from '../hooks/usePosts';
import { PostCard } from './components/PostCard';

const PostsPage = () => {
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState('');

  const { res } = usePosts(page, search);

  const onSearchChange = ({ target }: ChangeEvent<HTMLInputElement>) => {
    setPage(1);
    setSearch(target.value);
  };

  let pageItems = [];
  if (res != undefined) {
    for (let number = 1; number <= res!.meta.last_page; number++) {
      pageItems.push(
        <Pagination.Item
          key={number}
          active={number === page}
          onClick={() => setPage(number)}
        >
          {number}
        </Pagination.Item>
      );
    }
  }

  return (
    <>
      <div className="mt-5">
        <Row>
          <Col xs={10}>
            <input
              type="text"
              className="mb-2 form-control"
              placeholder="Buscar post"
              value={search}
              onChange={onSearchChange}
            />
          </Col>
          <Col>
            <Link to="/new">
              <Button variant="primary">Agregar</Button>
            </Link>
          </Col>
        </Row>

        <Row xs={1} md={2} className="g-4 mt-2">
          {res?.data.map((post) => (
            <PostCard key={post.id} post={post} />
          ))}
        </Row>

        <Pagination className="mt-5">{pageItems}</Pagination>
      </div>
    </>
  );
};

export default PostsPage;
