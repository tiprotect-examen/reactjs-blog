import Spinner from 'react-bootstrap/Spinner';
import { useParams } from 'react-router-dom';
import { usePost } from '../hooks/usePost';

const PostPage = () => {
  const { id } = useParams();

  const { isLoading, post } = usePost(id!);

  if (isLoading) {
    return (
      <Spinner animation="border" role="status">
        <span className="visually-hidden">Cargando...</span>
      </Spinner>
    );
  }

  return (
    <div className="mt-3">
      <h3>{post?.title}</h3>
      <h6 className="text-muted">Autor: {post?.author}</h6>
      <p>{post?.content}</p>
    </div>
  );
};

export default PostPage;
