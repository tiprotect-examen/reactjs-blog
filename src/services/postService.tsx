import axios from 'axios';
import {
  IPost,
  IPostPaginatedResponse,
} from '../interfaces/postPaginatedResponse';

const postService = axios.create({
  baseURL: 'http://localhost:8000/api',
});

export const fetchPaginate = async (
  page: number,
  search: string = ''
): Promise<IPostPaginatedResponse> => {
  const resp = await postService.get(`/posts?page=${page}&q=${search}`);
  return resp.data;
};

export const fetchPost = async (id: string): Promise<IPost> => {
  const resp = await postService.get(`/posts/${id}`);
  return resp.data.data;
};

export const addPost = async (data: any): Promise<IPost> => {
  const resp = await postService.post('/posts', data);
  return resp.data.data;
};
