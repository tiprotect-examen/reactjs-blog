import { createBrowserRouter } from 'react-router-dom';
import PostPage from '../pages/PostPage';
import PostsPage from '../pages/PostsPage';
import App from '../App';
import NewPost from '../pages/NewPost';

export const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      {
        path: '/',
        element: <PostsPage />,
      },
      {
        path: '/new',
        element: <NewPost />,
      },
      {
        path: '/:id',
        element: <PostPage />,
      },
    ],
  },
]);
